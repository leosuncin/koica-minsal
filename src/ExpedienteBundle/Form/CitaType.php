<?php

namespace ExpedienteBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CitaType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('fecha', 'datetime')
            ->add('paciente', 'Symfony\Bridge\Doctrine\Form\Type\EntityType', array(
                'class' => 'ExpedienteBundle\Entity\Expediente',
                'property' => 'dui',
            ))
            ->add('especialidad', 'Symfony\Bridge\Doctrine\Form\Type\EntityType', array(
                'class' => 'ExpedienteBundle\Entity\Especialidad',
                'property' => 'nombre',
            ))
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ExpedienteBundle\Entity\Cita'
        ));
    }
}
