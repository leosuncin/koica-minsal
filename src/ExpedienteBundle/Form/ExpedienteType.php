<?php

namespace ExpedienteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ExpedienteType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombreCompleto')
            ->add('fechaNacimiento', 'Symfony\Component\Form\Extension\Core\Type\BirthdayType')
            ->add('dui', null, array('attr' => array('pattern', '\\d{8}-\\d1')))
            ->add('direccion')
            ->add('telefono', null, array('attr' => array('pattern' => '\\d{4}-\\d{4}')))
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ExpedienteBundle\Entity\Expediente'
        ));
    }
}
