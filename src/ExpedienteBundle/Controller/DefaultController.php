<?php

namespace ExpedienteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/koica")
     */
    public function indexAction()
    {
        return $this->render('ExpedienteBundle:Default:index.html.twig');
    }
}
