<?php

namespace ExpedienteBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use ExpedienteBundle\Entity\Cita;
use ExpedienteBundle\Form\CitaType;

/**
 * Cita controller.
 *
 * @Route("/koica/cita")
 */
class CitaController extends Controller
{
    /**
     * Lists all Cita entities.
     *
     * @Route("/", name="koica_cita_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $citas = $em->getRepository('ExpedienteBundle:Cita')->findAll();

        return $this->render('ExpedienteBundle:Cita:index.html.twig', array(
            'citas' => $citas,
        ));
    }

    /**
     * Creates a new Cita entity.
     *
     * @Route("/new", name="koica_cita_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $citum = new Cita();
        $form = $this->createForm('ExpedienteBundle\Form\CitaType', $citum);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($citum);
            $em->flush();

            return $this->redirectToRoute('koica_cita_show', array('id' => $citum->getId()));
        }

        return $this->render('ExpedienteBundle:Cita:new.html.twig', array(
            'citum' => $citum,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Cita entity.
     *
     * @Route("/{id}", name="koica_cita_show")
     * @Method("GET")
     */
    public function showAction(Cita $citum)
    {
        $deleteForm = $this->createDeleteForm($citum);

        return $this->render('ExpedienteBundle:Cita:show.html.twig', array(
            'citum' => $citum,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Cita entity.
     *
     * @Route("/{id}/edit", name="koica_cita_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Cita $citum)
    {
        $deleteForm = $this->createDeleteForm($citum);
        $editForm = $this->createForm('ExpedienteBundle\Form\CitaType', $citum);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($citum);
            $em->flush();

            return $this->redirectToRoute('koica_cita_edit', array('id' => $citum->getId()));
        }

        return $this->render('ExpedienteBundle:Cita:edit.html.twig', array(
            'citum' => $citum,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Cita entity.
     *
     * @Route("/{id}", name="koica_cita_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Cita $citum)
    {
        $form = $this->createDeleteForm($citum);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($citum);
            $em->flush();
        }

        return $this->redirectToRoute('koica_cita_index');
    }

    /**
     * Creates a form to delete a Cita entity.
     *
     * @param Cita $citum The Cita entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Cita $citum)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('koica_cita_delete', array('id' => $citum->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
