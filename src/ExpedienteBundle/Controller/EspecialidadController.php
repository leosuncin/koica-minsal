<?php

namespace ExpedienteBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use ExpedienteBundle\Entity\Especialidad;
use ExpedienteBundle\Form\EspecialidadType;

/**
 * Especialidad controller.
 *
 * @Route("/koica/especialidad")
 */
class EspecialidadController extends Controller
{
    /**
     * Lists all Especialidad entities.
     *
     * @Route("/", name="koica_especialidad_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $especialidads = $em->getRepository('ExpedienteBundle:Especialidad')->findAll();

        return $this->render('ExpedienteBundle:Especialidad:index.html.twig', array(
            'especialidads' => $especialidads,
        ));
    }

    /**
     * Creates a new Especialidad entity.
     *
     * @Route("/new", name="koica_especialidad_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $especialidad = new Especialidad();
        $form = $this->createForm('ExpedienteBundle\Form\EspecialidadType', $especialidad);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($especialidad);
            $em->flush();

            return $this->redirectToRoute('koica_especialidad_show', array('id' => $especialidad->getId()));
        }

        return $this->render('ExpedienteBundle:Especialidad:new.html.twig', array(
            'especialidad' => $especialidad,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Especialidad entity.
     *
     * @Route("/{id}", name="koica_especialidad_show")
     * @Method("GET")
     */
    public function showAction(Especialidad $especialidad)
    {
        $deleteForm = $this->createDeleteForm($especialidad);

        return $this->render('ExpedienteBundle:Especialidad:show.html.twig', array(
            'especialidad' => $especialidad,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Especialidad entity.
     *
     * @Route("/{id}/edit", name="koica_especialidad_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Especialidad $especialidad)
    {
        $deleteForm = $this->createDeleteForm($especialidad);
        $editForm = $this->createForm('ExpedienteBundle\Form\EspecialidadType', $especialidad);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($especialidad);
            $em->flush();

            return $this->redirectToRoute('koica_especialidad_edit', array('id' => $especialidad->getId()));
        }

        return $this->render('ExpedienteBundle:Especialidad:edit.html.twig', array(
            'especialidad' => $especialidad,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Especialidad entity.
     *
     * @Route("/{id}", name="koica_especialidad_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Especialidad $especialidad)
    {
        $form = $this->createDeleteForm($especialidad);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($especialidad);
            $em->flush();
        }

        return $this->redirectToRoute('koica_especialidad_index');
    }

    /**
     * Creates a form to delete a Especialidad entity.
     *
     * @param Especialidad $especialidad The Especialidad entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Especialidad $especialidad)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('koica_especialidad_delete', array('id' => $especialidad->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
