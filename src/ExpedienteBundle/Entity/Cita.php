<?php

namespace ExpedienteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Cita
 *
 * @ORM\Table(name="cita")
 * @ORM\Entity(repositoryClass="ExpedienteBundle\Repository\CitaRepository")
 * @UniqueEntity({"fecha", "cita", "paciente"})
 */
class Cita
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="datetime")
     * @Assert\NotNull()
     * @Assert\Date()
     */
    private $fecha;

    /**
     * @var \ExpedienteBundle\Entity\Expediente
     *
     * @ORM\ManyToOne(targetEntity="ExpedienteBundle\Entity\Expediente")
     * @ORM\JoinColumn(name="paciente_fk", nullable=false)
     * @Assert\NotNull()
     */
    private $paciente;

    /**
     * @var \ExpedienteBundle\Entity\Especialidad
     *
     * @ORM\ManyToOne(targetEntity="ExpedienteBundle\Entity\Especialidad")
     * @ORM\JoinColumn(name="especialidad_fk", nullable=false)
     * @Assert\NotNull()
     */
    private $especialidad;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return Cita
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set paciente
     *
     * @param \ExpedienteBundle\Entity\Expediente $paciente
     * @return Cita
     */
    public function setPaciente(Expediente $paciente)
    {
        $this->paciente = $paciente;

        return $this;
    }

    /**
     * Get paciente
     *
     * @return \ExpedienteBundle\Entity\Expediente
     */
    public function getPaciente()
    {
        return $this->paciente;
    }

    /**
     * Set especialidad
     *
     * @param \ExpedienteBundle\Entity\Especialidad $especialidad
     * @return Cita
     */
    public function setEspecialidad(Especialidad $especialidad)
    {
        $this->especialidad = $especialidad;

        return $this;
    }

    /**
     * Get especialidad
     *
     * @return \ExpedienteBundle\Entity\Especialidad
     */
    public function getEspecialidad()
    {
        return $this->especialidad;
    }
}
